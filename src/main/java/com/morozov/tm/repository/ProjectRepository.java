package com.morozov.tm.repository;

import com.morozov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements Repository<Project> {
    private List<Project> projectList = new ArrayList<>();

    @Override
    public List<Project> readAll() {
        return projectList;
    }

    @Override
    public Project read(String name) {
        Project project = null;
        if (name != null || !name.isEmpty()) {
            for (Project p : projectList) {
                if (p.getProjectName().equals(name))
                    project = p;
            }
        }
        return project;
    }

    @Override
    public void write(String name) {
        projectList.add(new Project(name));
    }

    @Override
    public void update(String oldName, String newName) {
        Project project = read(oldName);
        if (project != null) project.setProjectName(newName);
    }

    @Override
    public void delete(String name) {
        Project project = read(name);
        if (project != null) projectList.remove(project);
    }

}
