package com.morozov.tm.service;

public class ApplicationStart {

   public static void startApp() {
        while (true) {
            ConsoleHelper.showHelpMenu();
            String console = ConsoleHelper.readString();
            switch (console) {
                case "1":
                    showProjectMenu();
                    break;
                case "2":
                    showTaskMenu();
                    break;
                default:
                    if (!"exit".equals(console)) {
                        ConsoleHelper.writeString("Команда не распознана, введите повторно");
                    }
            }
            if ("exit".equals(console)) break;
        }
    }

    private static void showProjectMenu() {
        while (true) {
            ProjectService.showAllProject();
            ConsoleHelper.showProjectCommandList();
            String console = ConsoleHelper.readString();
            switch (console) {
                case "1":
                    ConsoleHelper.writeString("Введите имя нового проекта");
                    String projectName = ConsoleHelper.readString();
                    ProjectService.writeProject(projectName);
                    break;
                case "2":
                    ConsoleHelper.writeString("Введите имя проекта для удаления");
                    String delProjectName = ConsoleHelper.readString();
                    ProjectService.deleteProject(delProjectName);
                    break;
                case "3":
                    ConsoleHelper.writeString("Введите имя проекта для обновления");
                    String oldProjectName = ConsoleHelper.readString();
                    ConsoleHelper.writeString("Введите новое имя проекта ");
                    String newProjectName = ConsoleHelper.readString();
                    ProjectService.updateProject(oldProjectName, newProjectName);
                    break;
                default:
                    if (!"exit".equals(console)){
                        ConsoleHelper.writeString("Введена неверная команда, введите повторно");
                    }
            }
            if ("exit".equals(console)) break;
        }
    }

    private static void showTaskMenu() {
        while (true) {
            TaskService.showAllTask();
            ConsoleHelper.showTaskCommandList();
            String console = ConsoleHelper.readString();
            switch (console) {
                case "1":
                    ConsoleHelper.writeString("Введите имя новой задачи");
                    String taskName = ConsoleHelper.readString();
                    TaskService.writeTask(taskName);
                    break;
                case "2":
                    ConsoleHelper.writeString("Введите имя задачи для удаления");
                    String delTaskName = ConsoleHelper.readString();
                    TaskService.deleteTask(delTaskName);
                    break;
                case "3":
                    ConsoleHelper.writeString("Введите имя задачи для обновления");
                    String oldTaskName = ConsoleHelper.readString();
                    ConsoleHelper.writeString("Введите новое имя задачи ");
                    String newTaskName = ConsoleHelper.readString();
                    TaskService.updateTask(oldTaskName, newTaskName);
                    break;
                default:
                    if (!"exit".equals(console)){
                        ConsoleHelper.writeString("Введена неверная команда, введите повторно");
                    }
            }
            if ("exit".equals(console)) break;
        }
    }
}
